# frozen_string_literal: true

# health check
module Rack
  # "lib/rack/health_check.rb"
  class HealthCheck
    def call(env)
      status = {
        redis: {
          connected: redis_connected
        },
        postgres: {
          connected: postgres_connected,
          migrations_updated: postgres_migrations_updated
        }
      }
      if redis_connected && postgres_connected && postgres_migrations_updated
        [200, {}, [status.to_json]]
      else
        [400, {}, [status.to_json]]
      end
    end

    protected

    def redis_connected
      unless defined?(::Redis)
        raise "Wrong configuration. Missing 'redis' gem"
      end
      res = ::Redis.new(url:ENV['REDIS_URL']).ping
      res == 'PONG' ? true : "Redis.ping returned #{res.inspect} instead of PONG"
    rescue StandardError
      false
    end

    def postgres_connected
      ApplicationRecord.establish_connection
      ApplicationRecord.connection
      ApplicationRecord.connected?
      ApplicationRecord.remove_connection
      return true
    rescue StandardError
      false
    end

    def postgres_migrations_updated
      return false unless postgres_connected

      !ApplicationRecord.connection.migration_context.needs_migration?
    end
  end
end
