# frozen_string_literal: true

# == Schema Information
#
# Table name: animes
#
#  id              :bigint(8)        not null, primary key
#  name            :string
#  synopsis        :text
#  sessions        :integer
#  number_episodes :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  tags            :hstore           is an Array
#

FactoryBot.define do
  factory :anime do
    sequence(:name) { |n| "anime_#{n}" }
    synopsis { 'MyText text text' }
    sessions { 1 }
    number_episodes { 1 }
    cover { fixture_file_upload(Rails.root.join('spec', 'support', 'assets', 'cover.jpg'), 'image/jpg') }
    banner { fixture_file_upload(Rails.root.join('spec', 'support', 'assets', 'banner.jpg'), 'image/jpg') }
  end
end
