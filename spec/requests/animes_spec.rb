# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Animes', type: :request do
  let(:admin_header) do
    {
      "Authorization": "bearer #{log_in_admin}"
    }
  end

  let(:user_header) do
    {
      "Authorization": "bearer #{log_in}"
    }
  end

  let(:invalid_header) do
    {
      "Authorization": 'bearer'
    }
  end

  let(:valid_attributes) do
    FactoryBot.build(:anime)
  end

  let(:tag) do
    {
      tags: {
        name: 'tag_name'
      }
    }
  end

  let(:update) do
    {
      name: 'new_name 123490450'
    }
  end

  describe 'GET /api/v1/animes' do
    context 'with valid token & admin' do
      FactoryBot.create_list(:anime, 30)
      before do
        get api_v1_animes_path, headers: admin_header, as: :json
      end

      it { expect(response).to have_http_status(:ok) }

      it 'response with current page' do
        headers = response.headers
        expect(headers['x-page']).to eq(1)
      end

      it 'response with correctly number records' do
        headers = response.headers
        expect(headers['x-total']).to eq(Anime.count)
      end

      it 'response with number records per page' do
        headers = response.headers
        expect(headers['x-per-page']).to eq(25)
      end

      it 'response with correctly number record on page' do
        json = JSON.parse(response.body)
        headers = response.headers
        expect(json['data'].length).to eq(Anime.page(headers['x-page']).count)
      end
    end
  end

  describe 'POST /api/v1/animes' do
    context 'with valid token & admin' do
      before do
        post api_v1_animes_path, headers: admin_header, params: valid_attributes, as: :json
      end

      it { expect(response).to have_http_status(:created) }

      it 'create to rcontroller' do
        anime = FactoryBot.build(:anime, name: 'new_name 123450')
        expect  do
          post api_v1_animes_path, headers: admin_header, params: anime, as: :json
        end.to change(Anime, :count).by(1)
      end
    end

    context 'with valid token' do
      before do
        post api_v1_animes_path, headers: user_header, params: valid_attributes, as: :json
      end

      it { expect(response).to have_http_status(:unauthorized) }

      it 'create to rcontroller' do
        anime = FactoryBot.build(:anime, name: 'new_name 123450')
        expect  do
          post api_v1_animes_path, headers: user_header, params: anime, as: :json
        end.to change(Anime, :count).by(0)
      end
    end

    context 'with invalid token' do
      before do
        post api_v1_animes_path, headers: user_header, params: { anime: valid_attributes }, as: :json
      end

      it { expect(response).to have_http_status(:unauthorized) }

      it 'create to rcontroller' do
        anime = FactoryBot.build(:anime, name: 'new_name 123450')
        expect  do
          post api_v1_animes_path, headers: user_header, params: { anime: anime }, as: :json
        end.to change(Anime, :count).by(0)
      end
    end
  end

  describe 'GET /api/v1/animes/:id' do
    context 'with valid token & admin' do
      let(:anime) { FactoryBot.create(:anime) }

      before do
        get api_v1_anime_path(anime), headers: admin_header, as: :json
      end

      it { expect(response).to have_http_status(:ok) }

      it 'response with category' do
        json = JSON.parse(response.body)
        expect(json['data']['id']).to eq(anime.id.to_s)
      end

      it 'Manda los atributos del anime' do
        json = JSON.parse(response.body)
        expect(json['data']['attributes'].keys).to contain_exactly('name', 'banner_medium_url',
                                                                   'banner_small_url', 'banner_url',
                                                                   'cover_medium_url', 'cover_small_url',
                                                                   'cover_url', 'created_at', 'sessions',
                                                                   'synopsis', 'updated_at')
      end
    end

    context 'with valid token & admin & invalid id' do
      before do
        get api_v1_anime_path(1111), headers: admin_header, as: :json
      end

      it { expect(response).to have_http_status(:not_found) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with valid token' do
      let(:anime) { FactoryBot.create(:anime) }

      before do
        get api_v1_anime_path(anime), headers: user_header, as: :json
      end

      it { expect(response).to have_http_status(:ok) }

      it 'response with category' do
        json = JSON.parse(response.body)
        expect(json['data']['id']).to eq(anime.id.to_s)
      end

      it 'Manda los atributos del anime' do
        json = JSON.parse(response.body)
        expect(json['data']['attributes'].keys).to contain_exactly('name', 'banner_medium_url',
                                                                   'banner_small_url', 'banner_url',
                                                                   'cover_medium_url', 'cover_small_url',
                                                                   'cover_url', 'created_at', 'sessions',
                                                                   'synopsis', 'updated_at')
      end
    end

    context 'with valid token & invalid id' do
      before do
        get api_v1_anime_path(1111), headers: user_header, as: :json
      end

      it { expect(response).to have_http_status(:not_found) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with invalid token' do
      anime = FactoryBot.create(:anime)
      before do
        get api_v1_anime_path(anime), headers: invalid_header, as: :json
      end

      it { expect(response).to have_http_status(:ok) }

      it 'response with category' do
        json = JSON.parse(response.body)
        expect(json['data']['id']).to eq(anime.id.to_s)
      end

      it 'Manda los atributos del anime' do
        json = JSON.parse(response.body)
        expect(json['data']['attributes'].keys).to contain_exactly('name', 'banner_medium_url',
                                                                   'banner_small_url', 'banner_url',
                                                                   'cover_medium_url', 'cover_small_url',
                                                                   'cover_url', 'created_at', 'sessions',
                                                                   'synopsis', 'updated_at')
      end
    end

    context 'with invalid token & invalid id' do
      before do
        get api_v1_anime_path(1111), headers: invalid_header, as: :json
      end

      it { expect(response).to have_http_status(:not_found) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end
  end

  describe 'PATCH  /api/v1/animes/:id' do
    context 'with valid token & admin' do
      anime = FactoryBot.create(:anime)
      before do
        patch api_v1_anime_path(anime), headers: admin_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:ok) }

      it 'update anime' do
        json = JSON.parse(response.body)
        expect(json['data']['attributes']['name']).to eq('new_name 123490450')
      end
    end

    context 'with valid token & admin & invalid id' do
      before do
        patch api_v1_anime_path(1111), headers: admin_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:not_found) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with valid token' do
      anime = FactoryBot.create(:anime)
      before do
        patch api_v1_anime_path(anime), headers: user_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:unauthorized) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with valid token & invalid id' do
      before do
        patch api_v1_anime_path(1111), headers: user_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:not_found) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with invalid token' do
      anime = FactoryBot.create(:anime)
      before do
        patch api_v1_anime_path(anime), headers: invalid_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:unauthorized) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with invalid token & invalid id' do
      before do
        patch api_v1_anime_path(1111), headers: invalid_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:unauthorized) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end
  end

  describe 'PUT  /api/v1/animes/:id' do
    context 'with valid token & admin' do
      anime = FactoryBot.create(:anime)
      before do
        put api_v1_anime_path(anime), headers: admin_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:ok) }

      it 'update anime' do
        json = JSON.parse(response.body)
        expect(json['data']['attributes']['name']).to eq('new_name 123490450')
      end
    end

    context 'with valid token & admin & invalid id' do
      before do
        @anime = FactoryBot.create(:anime)
        put api_v1_anime_path(1111), headers: admin_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:not_found) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with valid token & invalid id' do
      before do
        put api_v1_anime_path(1111), headers: user_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:not_found) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with valid token' do
      anime = FactoryBot.create(:anime)
      before do
        put api_v1_anime_path(anime), headers: user_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:unauthorized) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with invalid token' do
      anime = FactoryBot.create(:anime)
      before do
        put api_v1_anime_path(anime), headers: invalid_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:unauthorized) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end

    context 'with invalid token & invalid id' do
      before do
        put api_v1_anime_path(1111), headers: invalid_header, params: update, as: :json
      end

      it { expect(response).to have_http_status(:unauthorized) }

      it 'responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end
    end
  end

  describe 'DELETE /api/v1/animes/:id' do
    context 'with valid token & admin' do
      it {
        anime = FactoryBot.create(:anime)
        delete api_v1_anime_path(anime.id), headers: admin_header, as: :json
        expect(response).to have_http_status(:no_content)
      }

      it 'Delete Anime' do
        anime = FactoryBot.create(:anime)
        expect do
          delete api_v1_anime_path(anime), headers: admin_header, as: :json
        end.to change(Anime, :count).by(-1)
      end
    end

    context 'with valid token & admin & invalid id' do
      it {
        delete api_v1_anime_path(1111), headers: admin_header, as: :json
        expect(response).to have_http_status(:not_found)
      }

      it 'Delete Anime' do
        expect do
          delete api_v1_anime_path(111), headers: admin_header, as: :json
        end.to change(Anime, :count).by(0)
      end
    end

    context 'with valid token' do
      let(:anime) { FactoryBot.create(:anime) }

      it {
        delete api_v1_anime_path(anime), headers: user_header, as: :json
        expect(response).to have_http_status(:unauthorized)
      }

      it 'Delete Anime' do
        anime_item = anime
        expect do
          delete api_v1_anime_path(anime_item), headers: user_header, as: :json
        end.to change(Anime, :count).by(0)
      end
    end

    context 'with valid token & invalid id' do
      let(:anime) { FactoryBot.create(:anime) }

      it {
        delete api_v1_anime_path(1111), headers: user_header, as: :json
        expect(response).to have_http_status(:not_found)
      }

      it 'Delete Anime' do
        anime_item = anime
        expect do
          delete api_v1_anime_path(anime_item), headers: user_header, as: :json
        end.to change(Anime, :count).by(0)
      end
    end

    context 'with invalid token' do
      let(:anime) { FactoryBot.create(:anime) }

      it {
        delete api_v1_anime_path(anime), headers: invalid_header, as: :json
        expect(response).to have_http_status(:unauthorized)
      }

      it 'Delete Anime' do
        anime_item = anime
        expect do
          delete api_v1_anime_path(anime_item), headers: invalid_header, as: :json
        end.to change(Anime, :count).by(0)
      end
    end

    context 'with invalid token & invalid id' do
      let(:anime) { FactoryBot.create(:anime) }

      it {
        delete api_v1_anime_path(1111), headers: invalid_header, as: :json
        expect(response).to have_http_status(:unauthorized)
      }

      it 'Delete Anime' do
        anime_item = anime
        expect do
          delete api_v1_anime_path(anime_item), headers: invalid_header, as: :json
        end.to change(Anime, :count).by(0)
      end
    end
  end
end
