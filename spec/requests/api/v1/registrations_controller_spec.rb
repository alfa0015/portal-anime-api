# frozen_string_literal: true

require 'rails_helper'
RSpec.describe Api::V1::RegistrationsController, type: :request do
  describe 'POST   /api/v1/users(.:format)' do
    context 'with datos validos' do
      user = FactoryBot.build(:user)
      before do
        post user_registration_path, params: {
          user: {
            email: user.email,
            password: user.password,
            password_confirmation: user.password_confirmation
          }
        }
      end

      it { expect(response).to have_http_status(:created) }

      it { change(User, :count).by(1) }

      it 'responds with user found or create' do
        json = JSON.parse(response.body)
        expect(json['email']).to eq(user.email)
      end
    end

    context 'with Email invalido' do
      before do
        user = FactoryBot.build(:user)
        post user_registration_path, params: {
          user: {
            email: 'root',
            password: user.password,
            password_confirmation: user.password_confirmation
          }
        }
      end

      it { expect(response).to have_http_status(:unprocessable_entity) }

      it { change(User, :count).by(0) }

      it 'with responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end

      it 'with responds with error email' do
        json = JSON.parse(response.body)
        expect(json['errors']['email']).not_to be_empty
      end
    end

    context 'with Email password invaludo' do
      user = FactoryBot.build(:user_failer)
      before do
        post user_registration_path, params: {
          user: {
            email: user.email,
            password: user.password,
            password_confirmation: user.password_confirmation
          }
        }
      end

      it { expect(response).to have_http_status(:unprocessable_entity) }

      it { change(User, :count).by(0) }

      it 'with responds with error' do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_empty
      end

      it 'with responds with error password' do
        json = JSON.parse(response.body)
        expect(json['errors']['password']).not_to be_empty
      end
    end
  end
end
