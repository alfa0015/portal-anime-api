# frozen_string_literal: true

# == Schema Information
#
# Table name: animes
#
#  id              :bigint(8)        not null, primary key
#  name            :string
#  synopsis        :text
#  sessions        :integer
#  number_episodes :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  tags            :hstore           is an Array
#

require 'rails_helper'

RSpec.describe Anime, type: :model do
  it { is_expected.to have_db_column(:name).of_type(:string) }
  it { is_expected.to have_db_column(:synopsis).of_type(:text) }
  it { is_expected.to have_db_column(:sessions).of_type(:integer) }
  it { is_expected.to have_db_column(:number_episodes).of_type(:integer) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:synopsis) }
  it { is_expected.to validate_presence_of(:sessions) }
  it { is_expected.to have_many(:episodes) }

  it { is_expected.to validate_uniqueness_of(:name) }

  it { is_expected.to validate_length_of(:name).is_at_least(5).on(:create) }
  it { is_expected.to validate_length_of(:synopsis).is_at_least(10).on(:create) }

  it { is_expected.to validate_numericality_of(:sessions).only_integer }
end
