# frozen_string_literal: true

# Clase padre de los modelos
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
