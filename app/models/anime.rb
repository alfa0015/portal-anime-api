# frozen_string_literal: true

# == Schema Information
#
# Table name: animes
#
#  id              :bigint(8)        not null, primary key
#  name            :string
#  synopsis        :text
#  sessions        :integer
#  number_episodes :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  tags            :hstore           is an Array
#

# Clase model de anime
class Anime < ApplicationRecord
  has_many :episodes, dependent: :destroy

  has_one_attached :cover
  delegate :filename, to: :cover, allow_nil: true
  has_one_attached :banner
  delegate :filename, to: :banner, allow_nil: true

  has_one_attached :cover
  delegate :filename, to: :cover, allow_nil: true
  has_one_attached :banner
  delegate :filename, to: :banner, allow_nil: true

  validates :name, presence: true
  validates :synopsis, presence: true
  validates :sessions, presence: true
  validates :number_episodes, presence: true
  validates :cover, presence: true
  validates :banner, presence: true

  validates :name, uniqueness: true

  validates :name, length: { minimum: 5 }
  validates :synopsis, length: { minimum: 10 }

  validates :sessions, numericality: { only_integer: true }
  validates :number_episodes, numericality: { only_integer: true }

  validates :cover, file_size: {
    less_than_or_equal_to: 500.kilobytes,
    message: 'avatar should be less than %{count}',
    if: -> { cover.attached? }
  }, file_content_type: {
    allow: %r{^image/.*},
    message: 'the file has to be an image',
    if: -> { cover.attached? }
  }
  validates :banner, file_size: {
    less_than_or_equal_to: 500.kilobytes,
    message: 'avatar should be less than %{count}',
    if: -> { cover.attached? }
  }, file_content_type: {
    allow: %r{^image/.*},
    message: 'the file has to be an image',
    if: -> { cover.attached? }
  }
end
