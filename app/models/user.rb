# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

# Clase modelo de Users
class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  after_create :assign_default_role

  has_many :tokens, class_name: 'Doorkeeper::AccessToken', foreign_key: :resource_owner_id, inverse_of: false,
                    dependent: :destroy
  has_one :token, -> { order 'created_at DESC' }, class_name: 'Doorkeeper::AccessToken',
                                                  foreign_key: :resource_owner_id, inverse_of: false,
                                                  dependent: :destroy

  has_many :posts, dependent: :destroy

  def assign_default_role
    add_role(:user) if roles.blank?
  end
end
