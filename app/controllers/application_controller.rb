# frozen_string_literal: true

# Clase padre de controladores
class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: { errors: exception }, status: :not_found
  end

  rescue_from ActionController::ParameterMissing do |exception|
    render json: { errors: "#{exception.param} is required" }, status: :unprocessable_entity
  end
  include CanCan::ControllerAdditions

  # Return error when is 401
  def doorkeeper_unauthorized_render_options(*)
    { json: { errors: 'Not authorized' } }
  end

  rescue_from CanCan::AccessDenied do |_exception|
    render json: { errors: 'Not authorized' }, status: :unauthorized
  end

  def current_user
    User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end

  # Method set page pagination
  def page
    @page ||= params[:page] || 1
  end

  # Method set number record per page
  def per_page
    @per_page ||= params[:per_page] || 25
  end

  # Method set headers pagination
  def pagination_header(resource)
    # print current page
    headers['x-page'] = page
    # print records per page
    headers['x-per-page'] = per_page
    # print total records
    headers['x-total'] = resource.total_count
    # print total pages
    total_count = resource.total_count.to_f
    per_page_count = per_page.to_f
    headers['x-page-total'] = (total_count / per_page_count).ceil
  end
end
