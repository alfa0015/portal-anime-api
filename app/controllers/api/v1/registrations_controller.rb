# frozen_string_literal: true

module Api
  module V1
    # Clase custom para registro de devise
    class RegistrationsController < Devise::RegistrationsController
      respond_to :json
    end
  end
end
