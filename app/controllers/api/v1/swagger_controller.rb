# frozen_string_literal: true

module Api
  module V1
    # Clase para configuracion de swagger
    class SwaggerController < ApplicationController
      def index
        # vista en json de la configuracion de swagger
      end
    end
  end
end
