# frozen_string_literal: true

module Api
  module V1
    # Clase controlador de animes
    class AnimesController < ApplicationController
      before_action :doorkeeper_authorize!, except: %i[index show episodes]
      load_and_authorize_resource
      before_action :set_anime, only: %i[show update destroy]

      # GET /animes
      # GET /animes.json
      def index
        animes = Anime.all.order(id: :desc).page(page).per(per_page)
        options = {
          links: {
            first: api_v1_animes_url(per_page: per_page),
            self: api_v1_animes_url(page: current_page, per_page: per_page),
            last: api_v1_animes_url(page: animes.total_pages, per_page: per_page)
          }
        }
        render json: AnimeSerializer.new(animes, options).serialized_json, status: :ok
        pagination_header(animes)
      end

      # GET /animes/1/episodes
      # GET /animes/1/episodes.json
      def episodes
        anime = Anime.find(params[:anime_id])
        episodes = anime.episodes.order(id: :desc)
        render json: EpisodeSerializer.new(episodes).serialized_json
      end

      # GET /animes/1
      # GET /animes/1.json
      def show
        render json: AnimeSerializer.new(@anime).serialized_json
      end

      # POST /animes
      # POST /animes.json
      def create
        @anime = Anime.new(anime_params)
        if @anime.save
          render json: AnimeSerializer.new(@anime).serialized_json, status: :created
        else
          render json: @anime.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /animes/1
      # PATCH/PUT /animes/1.json
      def update
        if @anime.update(anime_params)
          render json: AnimeSerializer.new(@anime).serialized_json, status: :ok
        else
          render json: @anime.errors, status: :unprocessable_entity
        end
      end

      # DELETE /animes/1
      # DELETE /animes/1.json
      def destroy
        @anime.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_anime
        @anime = Anime.find(params[:id])
      end

      def anime_params
        params.permit(:name, :synopsis, :sessions, :number_episodes, :cover, :banner)
      end

      # metodo para devolver la pagina
      def current_page
        (params[:page] || 1).to_i
      end

      # metodo para devolver el numero por pagina
      def per_page
        (params[:per_page] || 25).to_i
      end
    end
  end
end
