# frozen_string_literal: true

# Clase base de envio de emails
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end
