# frozen_string_literal: true

# == Schema Information
#
# Table name: animes
#
#  id              :bigint(8)        not null, primary key
#  name            :string
#  synopsis        :text
#  sessions        :integer
#  number_episodes :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  tags            :hstore           is an Array
#

# Clase serializadora de Anime
class AnimeSerializer
  include FastJsonapi::ObjectSerializer
  set_type :anime
  attributes :name, :synopsis, :sessions, :created_at, :updated_at
  has_many :episodes, if: Proc.new { |anime| anime.episodes.any? }, links: {
    related: ->(anime) {
      Rails.application.routes.url_helpers.api_v1_anime_episodes_url(anime.id)
    }
  }

  link :self do |anime|
    Rails.application.routes.url_helpers.api_v1_anime_url(anime.id)
  end

  attribute :cover_url do |anime|
    if Rails.env.development? || Rails.env.test?
      Rails.application.routes.url_helpers.url_for(anime.cover) if anime.cover.attached?
    elsif anime.cover.attached?
      anime.cover.service_url
    end
  end
  attribute :cover_medium_url do |anime|
    if Rails.env.development? || Rails.env.test?
      Rails.application.routes.url_helpers.url_for(anime.cover.variant(resize: '400x400')) if anime.cover.attached?
    elsif anime.cover.attached?
      anime.cover.variant(resize: '400x400').processed.service_url
    end
  end
  attribute :cover_small_url do |anime|
    if Rails.env.development? || Rails.env.test?
      Rails.application.routes.url_helpers.url_for(anime.cover.variant(resize: '200x200')) if anime.cover.attached?
    elsif anime.cover.attached?
      anime.cover.variant(resize: '200x200').processed.service_url
    end
  end
  attribute :banner_url do |anime|
    if Rails.env.development? || Rails.env.test?
      Rails.application.routes.url_helpers.url_for(anime.banner) if anime.banner.attached?
    elsif anime.banner.attached?
      anime.banner.service_url
    end
  end
  attribute :banner_medium_url do |anime|
    if Rails.env.development? || Rails.env.test?
      Rails.application.routes.url_helpers.url_for(anime.banner.variant(resize: '400x400')) if anime.banner.attached?
    elsif anime.banner.attached?
      anime.banner.variant(resize: '400x400').processed.service_url
    end
  end
  attribute :banner_small_url do |anime|
    if Rails.env.development? || Rails.env.test?
      Rails.application.routes.url_helpers.url_for(anime.banner.variant(resize: '200x200')) if anime.banner.attached?
    elsif anime.banner.attached?
      anime.banner.variant(resize: '200x200').processed.service_url
    end
  end
end
