# frozen_string_literal: true

# WebSockets Episodes Clase
class EpisodesChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'episodes'
  end

  def unsubscribed
    # Cualquier limpieza necesaria cuando el canal se da de baja
  end

  def speak(data)
    # Cualquiera que envie datos al ws
  end
end
